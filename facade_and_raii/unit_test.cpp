///
/// \file
/// \author Daniel Staśczak
///

#include "spi.hpp"
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CRTP Test
#include "boost/test/unit_test.hpp"

BOOST_AUTO_TEST_CASE(facade_and_raii) {
    using namespace controller::d2xxwrapper;
    const Spi::Config config0{0, 15000, 0, 0};
    Spi::Spi spi0(config0);
    BOOST_CHECK_THROW(Spi::Spi{config0}, Spi::OpenPortException);

    const Spi::Config config1{1, 15000, 0, 0};
    {
        Spi::Spi spi1(config1);
    }
    Spi::Spi spi1(config1);

    constexpr DWORD bytes_count = 1024;
    BOOST_CHECK_EQUAL(bytes_count, spi1.read(bytes_count).size());
}
