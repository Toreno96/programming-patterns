///
/// \file
/// \brief Wrapper for SPI communication using D2XX library.
/// \author Daniel Staśczak
/// \author Marcin Orczyk
///

#ifndef _WRAPPERS_D2XX_SPI_H_
#define _WRAPPERS_D2XX_SPI_H_

#include <cstdint>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>
#include "third_party/d2xx/ftd2xx.h"

namespace controller {

namespace d2xxwrapper {

///
/// \brief Wrapper class for SPI communication using D2XX library.
///
/// Wraps the C library in easier to use, object-oriented interface.
/// Implementation ensures that all acquired resources (open ports) will be
/// automatically freed during destruction of the object.
class Spi {
    public:
        ///
        /// \brief Exception signaling failure during port opening.
        ///
        class OpenPortException : public std::runtime_error {
            public:
                OpenPortException();
                OpenPortException(int port);
                OpenPortException(int port, FT_STATUS status);
        };
        ///
        /// \brief Exception signaling failure during read from SPI device.
        ///
        class ReadException : public std::runtime_error {
            public:
                ReadException();
                ReadException(FT_STATUS status);
                ReadException(unsigned attempts, FT_STATUS status);
        };
        ///
        /// \brief Exception signaling failure during write to SPI device.
        ///
        class WriteException : public std::runtime_error {
            public:
                WriteException();
                WriteException(FT_STATUS status);
        };
        ///
        /// \brief Exception signaling failure with MPSSE.
        ///
        class MpsseFailedException : public std::runtime_error {
            public:
                MpsseFailedException();
                MpsseFailedException(const std::string& description);
        };
        ///
        /// \brief Configuration of the SPI device.
        ///
        struct Config {
            const int port;
            const int frequency;
            const DWORD readTimeout;
            const DWORD writeTimeout;
        };
        using Byte = uint8_t;
        using Bytes = std::vector<Byte>;
        Spi() = delete;
        Spi(const Config& config);
        ~Spi();
        Spi(const Spi&) = delete;
        Spi(Spi&& other);
        Spi& operator=(const Spi&) = delete;
        Spi& operator=(Spi&& other);
        ///
        /// \brief Reads specified count of bytes from SPI device.
        /// \param bytesCount count of bytes to read.
        /// \return Sequence of bytes read from SPI device.
        /// \throw ReadException
        ///
        Bytes read(DWORD bytesCount);
        ///
        /// \brief Writes specified bytes to SPI device.
        /// \param bytes sequence of bytes function should write to SPI device.
        /// \throw WriteException
        ///
        void write(const Bytes& bytes);
        Bytes transfer(const Bytes& bytes);
    private:
        void ftdiWrite(const Bytes& bytes);
        void initializeMpsse(const Config& config);
        void disableBy5ClockDivider();
        void setClockDivisor(const Config& config);
        void checkMpsseOperability();
        void disconnectLoopbackMode();
        void initializePins();
        void setChipSelect(bool state);
        int port_;
        FT_HANDLE ftHandle_;
        static std::unordered_set<int> usedPorts_;
};

} // namespace d2xxwrapper

} // namespace controller

#endif // #ifndef _WRAPPERS_D2XX_SPI_H_
