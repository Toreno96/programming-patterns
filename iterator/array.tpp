///
/// \file
/// \author Daniel Staśczak
///

#include "array.hpp"

template <class T, unsigned Size>
bool Array<T, Size>::Iterator::valid() const {
    return index_ < array_->size();
}

template <class T, unsigned Size>
void Array<T, Size>::Iterator::next() {
    ++index_;
}

template <class T, unsigned Size>
T Array<T, Size>::Iterator::get() const {
    return array_->array_[index_];
}

template <class T, unsigned Size>
Array<T, Size>::Iterator::Iterator(Array& array) : array_(&array), index_(0) {}

template <class T, unsigned Size>
Array<T, Size>::Array(const T* array) : array_(array) {}

template <class T, unsigned Size>
constexpr unsigned Array<T, Size>::size() const {
    return Size;
}

template <class T, unsigned Size>
typename Array<T, Size>::Iterator Array<T, Size>::first() {
    return Iterator(*this);
}
