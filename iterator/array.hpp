///
/// \file
/// \author Daniel Staśczak
///

#pragma once

#include "iiterator.hpp"

///
/// \brief Simple implementation of the fixed-length array.
///
template <class T, unsigned Size>
class Array {
    public:
        ///
        /// \brief Interface implementation of IITerator, specialized for Array.
        ///
        class Iterator : public IIterator<T> {
            friend class Array;
            public:
                bool valid() const override;
                void next() override;
                T get() const override;
            private:
                Iterator(Array& array);
                const Array* array_;
                unsigned index_;
        };
        ///
        /// \brief A constructor.
        ///
        Array(const T* array);
        ///
        /// \brief Returns size of the array.
        ///
        constexpr unsigned size() const;
        ///
        /// \brief Gets iterator to the first element.
        ///
        Iterator first();
    private:
        const T* array_;
};

#include "array.tpp"
