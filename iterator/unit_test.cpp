///
/// \file
/// \author Daniel Staśczak
///

#include "array.hpp"
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CRTP Test
#include "boost/test/unit_test.hpp"

BOOST_AUTO_TEST_CASE(iterator) {
    constexpr unsigned Size = 3;
    const int array[] = {1, 2, 3};
    BOOST_CHECK_EQUAL(Size, sizeof(array) / sizeof(int));
    Array<int, Size> arr(array);

    auto it = arr.first();
    auto is_same = std::is_same<decltype(it), decltype(arr)::Iterator>::value;
    BOOST_CHECK(is_same);

    for (int i = 1; i <= static_cast<int>(Size); ++i) {
        BOOST_CHECK(it.valid());
        BOOST_CHECK_EQUAL(it.get(), i);
        it.next();
    }
    BOOST_CHECK(!(it.valid()));
}

