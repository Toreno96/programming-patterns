///
/// \file
/// \author Daniel Staśczak
///

#pragma once

///
/// \brief Interface of the implementation of the Iterator pattern.
///
template <class T>
class IIterator {
    public:
        ///
        /// \brief Checks if iterator is currently pointing to the valid value.
        /// \return True if iterator is pointing to the valid value, false otherwise.
        ///
        virtual bool valid() const = 0;
        ///
        /// \brief Increments iterator.
        ///
        virtual void next() = 0;
        ///
        /// \brief Gets the value to which iterator is currently pointing.
        /// \return Value pointed to by iterator.
        ///
        /// Behaviour is undefined if function is called when iterator is
        /// pointing to the invalid value.
        ///
        virtual T get() const = 0;
};
