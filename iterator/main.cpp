#include "array.hpp"
#include <cassert>

int main() {
    constexpr unsigned Size = 3;
    const int array[] = {1, 2, 3};
    assert(Size == sizeof(array) / sizeof(int));
    Array<int, Size> arr(array);

    auto it = arr.first();
    for (int i = 1; i <= static_cast<int>(Size); ++i) {
        assert(it.valid() && it.get() == i);
        it.next();
    }
    assert(!(it.valid()));
}
