///
/// \file
/// \author Daniel Staśczak
///

#include "instances_counter.hpp"
#include "non_crtp_instances_counter.hpp"
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CRTP Test
#include "boost/test/unit_test.hpp"

struct Foo : public InstancesCounter<Foo> {};
struct Bar : public InstancesCounter<Bar> {};

BOOST_AUTO_TEST_CASE(crtp) {
    BOOST_CHECK_EQUAL(Foo::instances_count(), 0);
    Foo f1;
    BOOST_CHECK_EQUAL(Foo::instances_count(), 1);
    {
        Foo f2;
        BOOST_CHECK_EQUAL(Foo::instances_count(), 2);
    }
    Foo f3;
    BOOST_CHECK_EQUAL(Foo::instances_count(), 2);
    Bar b;
    BOOST_CHECK_EQUAL(Foo::instances_count(), 2);
    BOOST_CHECK_EQUAL(Bar::instances_count(), 1);
}

struct Spam : public NonCrtpInstancesCounter {};
struct Ham : public NonCrtpInstancesCounter {};

BOOST_AUTO_TEST_CASE(non_crtp) {
    BOOST_CHECK_EQUAL(Spam::instances_count(), 0);
    Spam f1;
    BOOST_CHECK_EQUAL(Spam::instances_count(), 1);
    {
        Spam f2;
        BOOST_CHECK_EQUAL(Spam::instances_count(), 2);
    }
    Spam f3;
    BOOST_CHECK_EQUAL(Spam::instances_count(), 2);
    Ham b;
    // When inheriting from the non-crtp implementation of InstancesCounter,
    // value of the static member variable count_ is shared between instances
    // of Spam and Ham class.
    BOOST_CHECK_EQUAL(Spam::instances_count(), 3);
    BOOST_CHECK_EQUAL(Ham::instances_count(), 3);
}
