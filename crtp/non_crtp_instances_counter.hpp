///
/// \file
/// \author Daniel Staśczak
///

#pragma once

class NonCrtpInstancesCounter {
    public:
        using CountType = unsigned;
        NonCrtpInstancesCounter();
        ~NonCrtpInstancesCounter();
        static CountType instances_count();
    private:
        static CountType count_;
};

