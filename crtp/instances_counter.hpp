///
/// \file
/// \brief Implementation of the instances counting functionality.
/// \author Daniel Staśczak
///

#pragma once

///
/// \brief Class implementing the instances counting functionality.
///
/// Inheriting from this class provides functionality of counting existing
/// instances to a subclass, automatically managed during calls to the
/// constructor and destructor.
///
template <class T>
class InstancesCounter {
    public:
        ///
        /// \brief Type of the variable that stores count information.
        ///
        using CountType = unsigned;
        ///
        /// \brief A constructor.
        ///
        InstancesCounter();
        ///
        /// \brief A destructor.
        ///
        ~InstancesCounter();
        ///
        /// \brief Gets count of existing instances.
        /// \return Count of total instances of the class that are currently alive.
        ///
        static CountType instances_count();
    private:
        static CountType count_;
};

#include "instances_counter.tpp"
