///
/// \file
/// \author Daniel Staśczak
///

#include "non_crtp_instances_counter.hpp"

NonCrtpInstancesCounter::CountType NonCrtpInstancesCounter::count_ = 0;

NonCrtpInstancesCounter::CountType NonCrtpInstancesCounter::instances_count() {
    return count_;
}

NonCrtpInstancesCounter::NonCrtpInstancesCounter() {
    ++count_;
}

NonCrtpInstancesCounter::~NonCrtpInstancesCounter() {
    --count_;
}

