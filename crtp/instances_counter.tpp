///
/// \file
/// \author Daniel Staśczak
///

#include "instances_counter.hpp"

template <class T>
typename InstancesCounter<T>::CountType InstancesCounter<T>::count_ = 0;

template <class T>
typename InstancesCounter<T>::CountType InstancesCounter<T>::instances_count() {
    return count_;
}

template <class T>
InstancesCounter<T>::InstancesCounter() {
    ++count_;
}

template <class T>
InstancesCounter<T>::~InstancesCounter() {
    --count_;
}
