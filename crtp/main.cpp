#include <cassert>
#include "instances_counter.hpp"

struct Foo : public InstancesCounter<Foo> {};
struct Bar : public InstancesCounter<Bar> {};

int main() {
    assert(Foo::instances_count() == 0);
    Foo f1;
    assert(Foo::instances_count() == 1);
    {
        Foo f2;
        assert(Foo::instances_count() == 2);
    }
    Foo f3;
    assert(Foo::instances_count() == 2);
    Bar b;
    assert(Foo::instances_count() == 2);
    assert(Bar::instances_count() == 1);
}
